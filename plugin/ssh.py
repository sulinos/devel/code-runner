import util
server = None
port = "22"
def deploy(variant):
    global server
    global port
    server=variant
    if ":" in variant:
        server = variant.split(":")[0]
        port = variant.split(":")[1]
    util.run("ssh-copy-id -p {} {}".format(port,server),quiet=True)

def call(command):
    return util.run_args(["ssh", "-p", port,  server, command])

def cleanup():
    return
