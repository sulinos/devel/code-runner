import os
import util
def deploy(variant):
    if not os.path.exists("Vagrantfile"):
        util.run("vagrant init {}".format(variant),quiet=True)
    util.run("vagrant up",quiet=True)

def call(command):
    return util.run_args(["vagrant", "ssh", "-c", command])

def cleanup():
    util.run("vagrant global-status --prune | grep -e \" $(pwd)\" | cut -f 1 -d \" \" | xargs vagrant destroy -f",quiet=True)
    util.run("rm -rf '.vagrant' '.vagrant.d' 'VirtualBox VMs' 'Vagrantfile'",quiet=True)
