# server.py
import http.server # Our http server handler for http requests
import socketserver # Establish the TCP Socket connections
import json # Parse json

import os, sys

from datetime import datetime
os.system("mkdir -p ~/.code-runner-build/")

class GiteaWebHook(http.server.SimpleHTTPRequestHandler):
    def do_POST(self):
        try:
            self.send_response(200)
            self.end_headers()
            self.wfile.write("OK".encode("utf-8"))
            data = self.rfile.read().decode("utf-8")
            j=json.loads(data)
            if "repository" not in j:
                return
            repo=j["repository"]["clone_url"]
            if "commits" not in j:
                return
            buildid = "{}/{}".format(os.path.basename(repo),j["commits"][0]["id"])
            os.system("code-runner {} {} & ".format(repo,buildid))
        except Exception as e:
            print("Invalid request!",file=sys.stderr)

PORT = 9000
Handler = GiteaWebHook
Handler.protocol_version = "HTTP/1.0"
with socketserver.TCPServer(("", PORT), Handler) as httpd:
    print("Http Server Serving at port", PORT)
    httpd.serve_forever()
